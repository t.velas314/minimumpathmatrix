import numpy as np

CSVData = open("matrix.csv")
arr = np.loadtxt(CSVData, delimiter=",")

def minimumCostPath(matrix):
    n = len(matrix)
    m = len(matrix[0])
    costs = [[0]*m for i in range(n)]
    costs[0][0] = matrix[0][0]
    for i in range(1, m):
        costs[0][i] = costs[0][i - 1] + matrix[0][i]
    for i in range(1, n):
        costs[i][0] = costs[i - 1][0] + matrix[i][0]
    for i in range(1, n):
        for j in range(1, m):
            costs[i][j] = min(costs[i - 1][j], costs[i][j - 1]) + matrix[i][j]
    #Finds a sequence of steps of the shortest path
    i = n - 1
    j = m - 1
    seq = []
    while i + j > 0:
        if j == 0:
            seq.append("D")
            i -= 1
        elif i == 0:
            seq.append("R")
            j -= 1
        elif costs[i - 1][j] < costs[i][j - 1]:
            seq.append("D")
            i -= 1
        else:
            seq.append("R")
            j -= 1
    print(seq[::-1])
    return costs[n - 1][m - 1]

print(minimumCostPath(arr))
