FROM python:3

ADD minimumpath.py /

RUN pip install pystrich numpy

CMD [ "python", "./minimumpath.py" ]